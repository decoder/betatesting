package core;

public class Bank {
	private BankAccount[] accounts;     // all the bank accounts at this bank
	private Customer[] customers;
	private int numOfAccounts = 5;      // the number of bank accounts at this bank
	private int numOfCustomers = 5;

	// Constructor: A new Bank object initially doesn’t contain any accounts.
	public Bank() {
	    accounts = new BankAccount[5];
	    customers = new Customer[5];
	    numOfCustomers = 0;
	    numOfAccounts = 0;
	    }

	// Creates a new bank account using the customer name and the opening balance given as parameters
	// and returns the account number of this new account. It also adds this account into the account list
	// of the Bank calling object.
	public int openNewAccount(Customer customer) {

	    BankAccount b = new BankAccount(customer);
	    accounts[numOfAccounts] = b;
	    numOfAccounts++;
	    return b.getAccountNum();
	}
	
	public Customer getBackCustomer(String customerName) {
		int i=0;
		boolean noEncontrado = true;
		while(i<numOfAccounts && noEncontrado) {
			if (customers[i].getName().equals(customerName)) noEncontrado=false;
			else i++;
		}
		if (i==numOfAccounts) return null;
		else return customers[i];
	}

	// Withdraws the given amount from the account whose account number is given. If the account is
	// not available at the bank, it should print a message.
	public void withdrawFrom(int accountNum, double amount) {
	    for (int i =0; i<numOfAccounts; i++) {
	        if (accountNum == accounts[i].getAccountNum()  ) {
	            accounts[i].debit(amount);
	            System.out.println("Amount withdrawn successfully");
	            return;
	        }
	    }
	    System.out.println("Account number not found.");
	    }

	// Deposits the given amount to the account whose account number is given. If the account is not
	// available at the bank, it should print a message.
	public void depositTo(int accountNum, double amount) {
	    for (int i =0; i<numOfAccounts; i++) {
	        if (accountNum == accounts[i].getAccountNum()  ) {
	            accounts[i].credit(amount);
	            System.out.println("Amount deposited successfully");
	            return;
	        }
	    }
	    System.out.println("Account number not found.");
	}

	// Prints the account number, the customer name and the balance of the bank account whose
	// account number is given. If the account is not available at the bank, it should print a message.
	public void printAccountInfo(int accountNum) {
	    for (int i =0; i<numOfAccounts; i++) {
	                if (accountNum == accounts[i].getAccountNum()  ) {
	                    System.out.println(accounts[i].getAccountInfo());
	                    return;
	                }
	            }
	    System.out.println("Account number not found.");
	}


	public void printAccountInfo(int accountNum, int n) {
	    for (int i =0; i<numOfAccounts; i++) {
	                        if (accountNum == accounts[i].getAccountNum()  ) {
	                            System.out.println(accounts[i].getAccountInfo());
	                            return;
	                        }
	                    }
	    System.out.println("Account number not found.");
	}

}


