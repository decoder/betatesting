package core;

public class BankAccount {

	   public static final int MAX_BALANCE = 1000; 
	   private /*@ spec_public @*/ double balance;
	   private /*@ spec_public @*/ boolean isLocked = false; 
	   private  /*@ spec_public @*/ Customer customer;
	   private  /*@ spec_public @*/ int accountNum;
	   
	   private static int numOfAccounts=0;

	   //@ public invariant balance >= 0 && balance <= MAX_BALANCE;

	   //@ assignable balance;
	   //@ ensures balance == 0;
	   public BankAccount(Customer customer) { 
		   this.customer = customer; 
		   balance = 0; 
		   accountNum = numOfAccounts;
		   numOfAccounts++;
	   }

	   //@ requires 0 < amount && amount + balance < MAX_BALANCE;
	   //@ assignable balance;
	   //@ ensures balance == \old(balance) + amount;
	   public void credit(double amount) { balance += amount; }

	   //@ requires 0 < amount && amount <= balance;
	   //@ assignable balance;
	   //@ ensures balance == \old(balance) - amount;
	   public void debit(double amount) { balance -= amount; }

	   //@ ensures isLocked == true;
	   public void lockAccount() { isLocked = true; }
	   
	  
	   public int getAccountNum() { return accountNum; }

	   public String getAccountInfo(){
           return "Account number: " + accountNum + "\nCustomer Name: " + customer.getName() + "\nBalance:" + balance +"\n";
       }
	   
	   public void printAccountInfo() { System.out.println("Balance de la cuenta: "+ balance); }
	   
	   //@   requires !isLocked;
	   //@   ensures \result == balance;
	   //@ also
	   //@   requires isLocked;
	   //@   signals_only BankingException;
	   public /*@ pure @*/ double getBalance()  {
	       if (!isLocked) { return balance; }
	       else { return -1; }
	   }
}
