package core;

public class Customer {
	private String name;
	private String dni;
	private String phone;
	
	
	public Customer(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public String getDni() {
		return dni;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
}
