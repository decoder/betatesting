# DECODER beta-testing app

To compile:

```
mvn clean install
```

To run application:

```
mvn exec:java -Dexec.mainClass="core.BankApp"
```

